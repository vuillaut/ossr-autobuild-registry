import os
import subprocess
import requests
from eossr.api import get_ossr_records


url_project = 'https://gitlab.in2p3.fr/api/v4/projects/14768/'
url_project_registry = url_project + 'registry/repositories'


def get_existing_registry_list(url=url_project_registry):
    req = requests.get(url)
    req.raise_for_status()

    req_json = req.json()
    if len(req_json) > 0:
        return [r['name'] for r in req_json]
    else:
        return []


class Image:

    def __init__(self, record):
        self.record = record
        pass

    @property
    def dir(self):
        try:
            image_dir = record.data['metadata']['relations']['version'][0]['parent']['pid_value']
        except:
            image_dir = str(record.id)
        return image_dir

    @property
    def name(self):
        return os.path.join(os.getenv('CI_REGISTRY_IMAGE'), self.dir)

    @property
    def tag(self):
        if 'version' in self.record.data['metadata']:
            image_tag = self.record.data['metadata']['version']
        else:
            image_tag = 'latest'
        return image_tag

    @property
    def full_path(self):
        return f"{self.name}:{self.tag}"




def build_ossr_container(record):

    image = Image(record)

    subprocess.run(['repo2docker', '--no-run',
                    '--image-name', image.name,
                    '--user-name', 'OSSR', '--user-id', str(record.id),
                    record.doi])
    subprocess.run(['docker', 'tag', f'{image.name}:latest', image.full_path])
    subprocess.run(['docker', 'push', image.full_path])

    return image.full_path


if __name__ == '__main__':

    ossr_kwargs = {}
    ossr_kwargs.setdefault('type', 'software')
    ossr_kwargs.setdefault('all_versions', False)
    records = get_ossr_records(**ossr_kwargs)

    existing_records_containers = get_existing_registry_list()

    for record in records:
        image = Image(record)
        if image.dir not in existing_records_containers:
            print(f"Building container for record:\n{record}")
            path = build_ossr_container(record)
            print(f"Container built. It can be found at {path}")

