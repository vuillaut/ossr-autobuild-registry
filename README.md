# OSSR registry


[![pipeline status](https://gitlab.in2p3.fr/escape2020/wp3/ossr-registry/ossr-autobuild-registry/badges/master/pipeline.svg)](https://gitlab.in2p3.fr/escape2020/wp3/ossr-registry/ossr-autobuild-registry/-/commits/master)



Registry for the OSSR.

**This is a test** automatically building containers for all software records in the OSSR using [`repo2docker`](https://repo2docker.readthedocs.io/en/latest/).

The list of containers can be found there: [https://escape2020.pages.in2p3.fr/wp3/ossr-registry/](https://escape2020.pages.in2p3.fr/wp3/ossr-autobuild-registry/ossr-registry/).
