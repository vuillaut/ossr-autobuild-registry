import os
import requests
from eossr.api.zenodo import get_record
from create_docker_containers import url_project_registry



def get_ossr_registry_dict():
    req = requests.get(url_project_registry+'?tags=1')
    req.raise_for_status()
    return req.json()


def record_html_summary(record):
    info = f"<h2> === {record.id} === </h2>"
    info += f"<h2>{record.title}</h2>"
    info += f"DOI: <a href={record.data['links']['html']}>{record.doi}</a><br>"
    if 'description' in record.data['metadata']:
        info += f"{record.data['metadata']['description']}"
    return info


def main():
    outdir = 'public'
    os.makedirs(outdir, exist_ok=True)

    # records_ids = get_existing_registry_list()
    registry_dict = get_ossr_registry_dict()

    outfile = os.path.join(outdir, 'index.html')

    with open(outfile, 'w') as file:
        file.write('<header> <h1> OSSR registry list </h1> </header>')

        for reg_entry in registry_dict:
            reg_id = reg_entry['id']
            record_id = reg_entry['name']
            record_tags = reg_entry['tags']
            if len(record_tags)>0:
                record = get_record(record_id)
                file.write(record_html_summary(record))
                file.write(f'<a href="https://gitlab.in2p3.fr/escape2020/wp3/ossr-registry/ossr-autobuild-registry/container_registry/{reg_id}">'
                           'Containers tags:</a><br>')
                for tag in record_tags:
                    file.write(f"<li> <code>{tag['location']}</code><br>")
                file.write("<br><br>")


if __name__ == '__main__':
    main()